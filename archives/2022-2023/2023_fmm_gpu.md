# Calcul d’interactions sur GPU par une méthode hiérarchique

Il s’agit de porter la méthode des multipôles rapide par interpolation (bibliothèque [ScalFMM](https://gitlab.inria.fr/solverstack/ScalFM) sur GPU au-dessus du moteur d’exécution [StarPU](https://starpu.gitlabpages.inria.fr/).

<!--more-->
La bibliothèque [ScalFMM](https://gitlab.inria.fr/solverstack/ScalFM) est un code C++ (14,17) moderne qui permet de calculer les interactions à l’aide d’une méthode d’interpolation. Notre approche est indépendante du noyau d’interaction et peut être appliquée à tout opérateur non oscillant (gravité, élasticité, gaussienne...) en dimension 1, 2, 3 et 4. L’algorithme hiérarchique se base sur un découpage de l’espace à l’aide d’un octree pour séparer le calcul du champ proche et du champ lointain. Le champ lointain est approché par une méthode d'interpolation. La parallélisation de la bibliothèque repose sur la programmation à base de tâches. Ce paradigme est une façon d'abstraire la couche matérielle pour déléguer la gestion bas-niveau de la parallélisation à un moteur d'exécution qui dispatchera les tâches sur les unités de calcul disponibles sur la machine. Pour une interpoaltion quelques noyaux de la bibliothèque ScalFMM ont été portés sur StarPU pour exploiter efficacement dans un premier temps un nœud de calcul multicœur et] éventuellement accéléré par des GPUs [2].

L’objectif de ce stage est de porter sur GPU l’ensemble des opérateurs impliqués dans le calcul du champ pour l’approche FMM par interpolation. Après s’être familiarisé avec la programmation à base de  tâches et l’algorithme de la FMM,  le travail commencera par optimiser (simd, blas …) les versions CPU des noyaux avant de les porter sur GPU.  L’objectif final est d’accélérer le calcul pour faire tourner rapidement des simulations de grande taille. L’implémentation sera réalisée en C++17 et devra respecter une architecture logicielle modulaire et maintenable. L’étude de performance sera réalisée sur la plate-forme PlaFRIM (2000 cœurs) et la machine Jean Zay de l’Idris.

Le stage sera gratifié, possibilité du remboursement de la moitié des frais de transport et participation aux frais de restauration.  Dans le cadre  de l'ANR DIWINA le stage pourra se prolonger par une thèse en septembre 2023 (financement acquis).
## Mots-clés: méthode multipôles rapide, N- corps, GPU, C++14

## Pré-requis: calcul parallèle. Connaissances en Cuda et C++

## Contacts : Olivier Coulaud (olivier.coulaud@inria.fr), Bérenger Bramas (berenger.bramas@inria.fr)

## Lieu du stage : Bordeaux
 La personne sera intégrée au sein du projet INRIA Bordeaux Concace, équipe projet commune Airbus CRT, Cerfacs et Inria et collaborera fortement avec l’équipe Inria Camus localisée à Strasbourg.

## Référence:
 [1]  Emmanuel Agullo, Berenger Bramas, Olivier Coulaud, Eric Darve, Matthias Messner, Toru Takahashi ;  Task-based FMM for heterogeneous architectures. Concurrency and Computation: Practice and Experience, 2016, 28 (9),


